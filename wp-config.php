<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lorendb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R?lA71^1(]M(E6]S?hv,Yte%.yFO2x^B7eR{=pJ,b?JUhp9c(wqN*Qme KXY(LUB');
define('SECURE_AUTH_KEY',  'qv?f2(tmE<w$.0F0XTMt,mcf]mnnB$T}n##V$i$OOPm9;L`@D=%]8,,XvV|j74](');
define('LOGGED_IN_KEY',    'uvM]%=}B2..qyFA2J,f<D#w#CO.U]m3iHZ_K+U!4rN(` (8|k/X[_hyi7x!,Fjb*');
define('NONCE_KEY',        '*Z&q}&nt+Jv2&+comS]qV;S3xk[ktg_9tJA-tJCid9xsbu:*d%sF/($>?$>MuB)w');
define('AUTH_SALT',        'QW(XW[1^,co 86: Q1~v9k53EF&BLh0i[kBS,*ApiWU?1~XLO[Bn-UDm%p:1m->L');
define('SECURE_AUTH_SALT', '(t&v/V;f,pKrP(HAQ=UXDb~}nenU{D0:p,po@i>Afh+wF6^l&K-c}]5NfU;U:7~q');
define('LOGGED_IN_SALT',   '$_SCyd$K?E^eT6 GIl.4YxP9>Y1Lo3|$Ifzu#sp CP~U)fHodcNnMy~&Yfwe`_cy');
define('NONCE_SALT',       '5PI1f.ALE><rd})G$y]j$HD8Ek>2n2U,)_R?NQH7I9-(9$tGiFL!^r`s+jNq(h,q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
